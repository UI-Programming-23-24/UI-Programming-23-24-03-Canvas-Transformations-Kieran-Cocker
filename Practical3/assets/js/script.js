console.log("This script is working");

// Getting the canvas element
const canvas = document.querySelector("#mainCanvas");
const context = canvas.getContext("2d");

// New function
function artGeneration() {
	for (let i = 0; i < 100; i++) { // Generate 100 shapes using a for loop
		// Generate random numbers and multiply 0 by the canvas width and height
		const x = Math.random() * canvas.width; 
		const y = Math.random() * canvas.height;
		
		const radius = Math.random() * 50; // Generate a random radius and multiply by 50
		const colour = `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 1}`; // Randomise the colour by randomizing between 0 and 256 for each hue
		
		context.beginPath(); // Beginning the shape path
		// Set the positions of all the circles
		context.arc(x, y, radius, 0, Math.PI * 2); // Make the shapes into circles by multiplying by PI * 2
		context.fillStyle = colour; // Filling the shapes with colours dictated by the random colour variable
		context.fill(); // Draw the shapes
	}
}

const squam = document.getElementById("squam"); // Getting the audio file

// Event listener
// Play audio is a custom event
squam.addEventListener('playAudio', function() { // Creating a function to call the audio control function
	audioControl();
});

// Audio function
function audioControl() {
	let times = 0; // Counter for amount of times audio will be played, if 0 sound will play
	
	// new function to play the audio
	function play() {
		// If sound has not been played 3 times
		if (times < 3) {
			squam.play(); // Play sound
			times++; // Count times the sound has been played up
			setTimeout(play, squam.duration * 1000); // Set an interval of 1 second between audio plays
		}
		
	}
	
	play(); // Call the function
}

artGeneration(); // Call the function